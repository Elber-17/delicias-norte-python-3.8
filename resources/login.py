from flask import request
from flask_jwt_extended import create_access_token
from sqlalchemy import func

from core.resource_core import BaseResource
from models.user import User

class Login(BaseResource):
	def admin_login(self, client_login=False):
		if not 'name' in request.args:
			return self.make_response({'Bad Request' : 'Falta el parametro name'}, 400)

		if not 'password' in request.args:
			return self.make_response({'Bad Request' : 'Falta el parametro password'}, 400)

		request_args = dict(request.args.copy())
		name = request_args['name']
		password = request_args['password']
		user = self.get_user(name, password)

		if not user:
			return self.make_response({'Not Found' : 'El nombre de usuario o contraseña es incorrecto'}, 404)

		id = user.id
		status = user.user_status_id
		level = user.user_level_id

		if status == -1:
			return self.make_response({'Unauthorized' : 'Este usuario fue eliminado'}, 401)

		if not client_login and level != 1:
			return self.make_response({'Unauthorized' : 'Este usuario no tiene los privilegios necesarios'}, 401)

		response = {
						'access_token' : create_access_token(id)
					}


		return self.make_response(response, 200)

	def get_user(self, name, password):
		# print(User.query.filter(User.name == func.binary(name), User.password == func.binary(password)).first())
		return User.query.filter_by(name=func.binary(name), password=func.binary(password)).first()

	def client_login(self):
		return self.admin_login(True)