from flask import request
from flask_jwt_extended import jwt_required
from flask_jwt_extended import get_jwt_claims

from core.resource_core import BaseResource
from models.user import User as ModelUser
from core.sqlalchemy import db


class User(BaseResource):
	@jwt_required
	def get(self):
		model_user = ModelUser()
		claims = get_jwt_claims()
		id = claims['id']
		user = model_user.query.get(id)
		response = {
			'name': user.name,
			'last_name': user.last_name,
			'cedula': user.cedula,
			'email': user.email,
			'password': user.password,
		}

		return self.make_response(response, 200)
	
	@jwt_required
	def patch(self):
		model_user = ModelUser()
		claims = get_jwt_claims()
		user_id = claims['id']
		user = model_user.query.get(user_id)
		args_to_update = self.validate_patch_args()
		
		if not args_to_update:
			response = {'Bad Request' : 'Debes enviar al menos un parametro para actualizar'}
			
			return self.make_response(response, 400)
		
		for arg in args_to_update:
			setattr(user, arg, request.args[arg])

		db.session.add(user)
		db.session.commit()

		return self.make_response({}, 204)

	
	def validate_patch_args(self):
		valid_args =  ['name', 'last_name', 'cedula', 'email', 'password']
		args_to_update = [arg for arg in valid_args if arg in request.args]

		return args_to_update if args_to_update else False
