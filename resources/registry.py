from flask import request
from flask.wrappers import Response
from sqlalchemy import null
from sqlalchemy.exc import IntegrityError

from traceback import print_exc

from core.resource_core import BaseResource
from models.user import User
from core.sqlalchemy import db

class Registry(BaseResource):
	def user_registry(self):
		validate = self.validate_args()
		integrity_error = True

		if type(validate) == Response:
			return validate

		response = {}
		request_args = dict(request.args.copy())
		request_args[self.get_primary_key()] = self.get_next_id()
		request_args['user_status_id'] = 1
		request_args['user_level_id'] = 1
		request_args['registry_token'] = self.create_registry_token()
		user = User(**request_args)

		db.session.add(user)

		while(integrity_error):
			try:
				db.session.commit()
				integrity_error = False
			except IntegrityError:
				db.session.rollback()
				request_args['registry_token'] = self.create_registry_token()
				user = User(**request_args)
				db.session.add(user)
				continue

			except Exception as msg:
				print_exc()
				return self.make_response(str(msg), 401)

		response['Status'] = 'CREATED'
		
		for key, value in request_args.items():
			response[key] = value

		return self.make_response(response, 201)
	
	def validate_args(self):
		if not 'name' in request.args:
			return self.make_response({'Bad Request' : 'Falta el parametro name'}, 400)

		if not 'password' in request.args:
			return self.make_response({'Bad Request' : 'Falta el parametro password'}, 400)

		return True
	
	def create_registry_token(self):
		from random import choice

		string = 'abcdefghijklmnopqrstuvwxyz1234567980ABCDEFGHIJKLMNOPQRSTUVWXYZ'
		registry_token = ''

		for i in range(128):
			registry_token += choice(string)

		return registry_token

	def get_primary_key(self):
		primary_key = list(User.__table__.c)[0]
		primary_key = str(primary_key)
		index = primary_key.index('.')
		primary_key = primary_key[index+1:]

		return primary_key

	def get_next_id(self):
		from sqlalchemy import func
		from sqlalchemy import select

		primary_key = list(User.__table__.c)
		result =  db.session.execute(select([func.max(primary_key[0])])).first()[0]

		if not result:
			return 1

		result += 1

		return result
	
	def validate_registry_token(self):
		if not request.args:
			return self.make_response("falta el parametro 'token'", 400)

		user = User.query.filter_by(registry_token=request.args['token']).first()
		
		if not user:
			return self.make_response("No existe usuario con el token de acceso '"  + request.args['token'] +"'", 404)
		else:
			response = {
				'name' : user.name, 
				'last_name' : user.last_name
				}
			return self.make_response(response, 200)