from flask import request
from flask_jwt_extended import jwt_required
from sqlalchemy.exc import IntegrityError, DataError

from core.resource_core import BaseResource
from models.product import Product as ModelProduct
from models.product import ProductCategory as ModelProductCategory
from core.sqlalchemy import db


class Product(BaseResource):
	def get(self):
		final_reponse = []
		model_product = ModelProduct()
		all_products = model_product.query.all()

		for product in all_products:
			response = {
				'id' : product.id,
				'name': product.name,
				'product_category': {
					'id' : product.product_category.id,
					'name' : product.product_category.name
				},
				'stock': product.stock,
				'price': str(product.price),
				'img_url': product.img_url
			}

			final_reponse.append(response)

		return self.make_response(final_reponse, 200)

	def get_categories(self):
		final_reponse = []
		model_product_category = ModelProductCategory()
		all_product_categories = model_product_category.query.all()

		for category in all_product_categories:
			response = {
				'id' : category.id,
				'name': category.name,
			}

			final_reponse.append(response)

		return self.make_response(final_reponse, 200)

	@jwt_required
	def patch(self):
		model_product = ModelProduct()
		product_id, args_to_update = self.validate_patch_args()

		if not product_id:
		 	return self.make_response({'Bad Request' : "Falta el parametro 'id'"},400)

		product = model_product.query.get(product_id)

		if not product:
			return self.make_response({'Bad Request' : 'No existe un producto con el id que has enviado'}, 400)
		
		if not args_to_update:
			return self.make_response({'Bad Request' : 'Debes enviar al menos un parametro del producto para actualizar'}, 400)
		
		for arg in args_to_update:
			setattr(product, arg, request.args[arg])
		
		db.session.add(product)
		
		try:
			db.session.commit()
		
		except (IntegrityError, DataError):
			db.session.rollback()
			return self.make_response({'Bad Request' : "el 'id' de la categoria que enviaste no existe"}, 400)

		return self.make_response({}, 204)


	def validate_patch_args(self):
		product_id = request.args.get('id', None)
		valid_args =  ['name', 'product_category_id', 'stock', 'price']
		args_to_update = [arg for arg in valid_args if arg in request.args]

		return product_id, args_to_update
	
	def update_img_url(self, image_name):
		product_id = request.args.get('id', None)
		model_product = ModelProduct()
		product = model_product.query.get(product_id)
		product.img_url = 'http://localhost:8888/product/' + image_name
		db.session.add(product)
		db.session.commit()

		return self.make_response({},204)