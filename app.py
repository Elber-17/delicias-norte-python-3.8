from flask import Flask
from flask import request
from flask import jsonify
from flask import send_from_directory
from flask_jwt_extended import JWTManager
from flask_jwt_extended import jwt_required
from flask_jwt_extended import get_jwt_claims
from flask_cors import CORS

from core.sqlalchemy import db
from config.db_config import URI
from resources.login import Login
from resources.log_out import LogOut
from resources.registry import Registry
from resources.product import Product
from resources.user import User
from errors.token_errors import TokenErrorHandler


app = Flask(__name__)
blacklist = set()

app.config['SQLALCHEMY_DATABASE_URI'] = URI
app.config['JWT_SECRET_KEY'] = 'super-secret'
app.config['JWT_ACCESS_TOKEN_EXPIRES'] = 60 * 60
app.config['JWT_BLACKLIST_ENABLED'] = True
app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access']
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db.init_app(app)
CORS(app)
jwt = JWTManager(app)
log_out = LogOut(blacklist)
jwt.token_in_blacklist_loader(log_out.check_if_token_in_blacklist)
jwt.revoked_token_loader(TokenErrorHandler.revoken_token)
jwt.unauthorized_loader(TokenErrorHandler.unauthorized_token)
jwt.expired_token_loader(TokenErrorHandler.expired_token)

@jwt.user_claims_loader
def add_claims_to_access_token(id):
    return {
        'id': id,
    }

@app.route('/check-access-token/', methods=['GET'])
@jwt_required
def check_access_token():
	return jsonify({'Status': 'Valid'}), 200

@app.route('/check-access-token-admin/', methods=['GET'])
@jwt_required
def check_access_token_admin():
	from models.user import User as ModelUser

	model_user = ModelUser()
	claims = get_jwt_claims()
	id = claims['id']
	user = model_user.query.get(id)

	if user.user_level_id != 1:
		return jsonify({'Status': 'Unauthorized'}), 401

	return jsonify({'Status': 'Valid'}), 200

@app.route('/admin-login/', methods=['POST'])
def login_admin():
	login = Login()

	return login.admin_login()

@app.route('/client-login/', methods=['POST'])
def login_client():
	login = Login()

	return login.client_login()

@app.route('/user-registry/', methods=['POST'])
def user_registry():
	registry = Registry()

	return registry.user_registry()

@app.route('/check-token-registry/', methods=['GET'])
def check_token_registro():
	registry = Registry()
	return registry.check_registry_token()

@app.route('/product/', methods=['GET', 'POST', 'PATCH', 'DELETE'])
def product():
	product = Product()
	
	if request.method == 'GET':
		return product.get()
	
	elif request.method == 'POST':
		return 'Yo voy a registrar productos'
	
	elif request.method == 'PATCH':
		return product.patch()
	
	elif request.method == 'PUT':
		return 'Yo voy a actualizar productos'
	
	elif request.method == 'DELETE':
		return 'Yo voy a eliminar productos'

@app.route('/product/categories/', methods=['GET'])
def product_categories():
	product = Product()

	return product.get_categories()

@app.route('/product/<img>', methods=['GET'])
def product_img(img):
	return send_from_directory('product-images', img)
	
@app.route('/client/', methods=['GET', 'PATCH'])
def client():
	client = User()

	if request.method == 'GET':
		return client.get()
	
	elif request.method == 'PATCH':
		return client.patch()

@app.route('/upload-product-image/', methods=['POST'])
def upload_product_img():
	from os import path as os_path

	product = Product()
	product_id = request.args.get('id', None)

	if not product_id:
		return jsonify({'Bad Request' : 'Falta el id del producto'}), 400

	image = request.files.get('image', None)
	image_extension = image.filename[image.filename.find('.'):] 
	path = os_path.join('product-images','product' + str(product_id) + image_extension)
	image.save(path)

	return product.update_img_url('product' + str(product_id) + image_extension)

@app.route('/log-out/', methods=['DELETE'])
@jwt_required
def salir():
	return log_out.logout()

if __name__ == "__main__":
	app.run(use_debugger=True, host= "0.0.0.0", port=8888, use_reloader=True)