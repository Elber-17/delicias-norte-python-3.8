from sqlalchemy import Column
from sqlalchemy.dialects.mysql import BIGINT
from sqlalchemy.dialects.mysql import VARCHAR
from sqlalchemy.dialects.mysql import TINYINT
from sqlalchemy.dialects.mysql import TIMESTAMP

from core.sqlalchemy import db
# from sqlalchemy.orm import validates


class User(db.Model):
    id = Column(BIGINT(unsigned=True), primary_key=True)
    name = Column(VARCHAR(32), nullable=False)
    last_name = Column(VARCHAR(64), nullable=True)
    email = Column(VARCHAR(1024), nullable=False)
    password = Column(VARCHAR(1024), nullable=False)
    cedula = Column(VARCHAR(16), nullable=True)
    user_level_id = Column(TINYINT(unsigned=True), db.ForeignKey('user_level.id'))
    user_status_id = Column(TINYINT(), db.ForeignKey('user_status.id'))
    creation_date = Column(TIMESTAMP())
    registry_token = Column(VARCHAR(128), nullable=False)
    last_login = Column(TIMESTAMP())
	
    user_level = db.relationship('UserLevel')
    user_status = db.relationship('UserStatus')

class UserLevel(db.Model):
    id = Column(TINYINT(unsigned=True), primary_key=True)
    code = Column(VARCHAR(32), nullable=False)
    description = Column(VARCHAR(64), nullable=False)

class UserStatus(db.Model):
    id = Column(TINYINT(), primary_key=True)
    code = Column(VARCHAR(32), nullable=False)
    description = Column(VARCHAR(64), nullable=False)
