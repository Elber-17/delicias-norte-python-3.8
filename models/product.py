from sqlalchemy import Column
from sqlalchemy.dialects.mysql import BIGINT
from sqlalchemy.dialects.mysql import VARCHAR
from sqlalchemy.dialects.mysql import TINYINT
from sqlalchemy.dialects.mysql import SMALLINT
from sqlalchemy.dialects.mysql import DECIMAL

from core.sqlalchemy import db


class Product(db.Model):
	id = Column(BIGINT(unsigned=True), primary_key=True)
	name = Column(VARCHAR(128), nullable=False)
	product_category_id = Column(TINYINT(unsigned=True), db.ForeignKey('product_category.id'))
	stock = Column(SMALLINT(unsigned=True))
	price = Column(DECIMAL(15,2))
	img_url = Column(VARCHAR(1024))

	product_category = db.relationship('ProductCategory')


class ProductCategory(db.Model):
	id = Column(TINYINT(unsigned=True), primary_key=True)
	name = Column(VARCHAR(128))