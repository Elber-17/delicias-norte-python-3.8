-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 29-02-2020 a las 01:42:45
-- Versión del servidor: 5.7.25-log
-- Versión de PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
--
-- Base de datos: `delicias-norte`

create database `delicias-norte` char set utf8mb4 collate utf8mb4_general_ci;

use `delicias-norte`

--
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `product`
--

CREATE TABLE `product` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(128) NOT NULL,
  `product_category_id` tinyint(3) UNSIGNED NOT NULL,
  `stock` smallint(5) UNSIGNED NOT NULL,
  `price` decimal(15,2) NOT NULL,
  `img_url` varchar(1024) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Truncar tablas antes de insertar `product`
--

TRUNCATE TABLE `product`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `product_category`
--

CREATE TABLE `product_category` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Truncar tablas antes de insertar `product_category`
--

TRUNCATE TABLE `product_category`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sales`
--

CREATE TABLE `sales` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `quantity` smallint(5) UNSIGNED NOT NULL,
  `sub_total` decimal(15,2) NOT NULL,
  `iva` tinyint(3) UNSIGNED NOT NULL,
  `datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Truncar tablas antes de insertar `sales`
--

TRUNCATE TABLE `sales`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `last_name` varchar(64) DEFAULT NULL,
  `email` varchar(1024) DEFAULT NULL,
  `password` varchar(1024) NOT NULL,
  `user_level_id` tinyint(3) UNSIGNED NOT NULL,
  `user_status_id` tinyint(4) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `registry_token` varchar(128) NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Truncar tablas antes de insertar `user`
--

TRUNCATE TABLE `user`;
--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `name`, `last_name`, `email`, `password`, `user_level_id`, `user_status_id`, `creation_date`, `registry_token`, `last_login`) VALUES
(1, 'admin', 'dsfadsf', 'dsfasd@corre.com', '1234', 1, 1, '2020-02-28 14:55:32', 'r9lXmCZUVGDFbfTBfJEgklzV9tlnc2YgHJYkJQXhxs8062x3wdzqilEWEyzikg6cgfrfILYaS44mDmCJlwOQGxoXyOo0xbRV1R1SIm5nVxlTea0PBuMYvya1xQs70hSw', '2020-02-28 14:55:32'),
(2, 'Elber', 'Nava', 'prueba@correo.com', 'dsffdasfasdfsa', 1, 1, '2020-02-28 19:14:56', 'fKKwLdNxkmD2Io2eGPhvmRLwVMmD8IhyhMjqQoThyimVNZbIxIktVQOfb8C73fO8pL3PYF2jIkJFKP2RZZeVGsGQw6xuron2LZj9e8Rrv4Dl20XH3sEmcJhVzCAg9kIj', '2020-02-28 19:14:56'),
(3, 'Jennel', 'dsfadsf', 'prueba@correo.com', 'sdfadsfadfsaAAa', 1, 1, '2020-02-29 00:29:28', 'R61M44XLdTWWgwOirO1PalG1MCkGfzPeJ6xnnZyXJoGHbZzuvi8tz0QimTe6jasODe9uddqZuT5IAbvRHuicMp7hrgZlQ1dzOvrDfl7NK0UU5exyhR7BgsyEwLEgjzfs', '2020-02-29 00:29:28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_level`
--

CREATE TABLE `user_level` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `code` varchar(16) NOT NULL,
  `description` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Truncar tablas antes de insertar `user_level`
--

TRUNCATE TABLE `user_level`;
--
-- Volcado de datos para la tabla `user_level`
--

INSERT INTO `user_level` (`id`, `code`, `description`) VALUES
(1, 'ADMIN', 'Usuario con acceso completo al sistema.'),
(2, 'CLIENTE', 'Usuario con acceso solo al modulo de compras.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_status`
--

CREATE TABLE `user_status` (
  `id` tinyint(4) NOT NULL,
  `code` varchar(16) NOT NULL,
  `description` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Truncar tablas antes de insertar `user_status`
--

TRUNCATE TABLE `user_status`;
--
-- Volcado de datos para la tabla `user_status`
--

INSERT INTO `user_status` (`id`, `code`, `description`) VALUES
(-1, 'ELIMINADO', 'Usuario eliminado, no puede usar el sistema'),
(1, 'ACTIVO', 'Usuario activo, puede utilizar el sistema');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_category_id` (`product_category_id`);

--
-- Indices de la tabla `product_category`
--
ALTER TABLE `product_category`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `token` (`registry_token`),
  ADD KEY `user_level_id` (`user_level_id`),
  ADD KEY `user_status_id` (`user_status_id`);

--
-- Indices de la tabla `user_level`
--
ALTER TABLE `user_level`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user_status`
--
ALTER TABLE `user_status`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `product_category`
--
ALTER TABLE `product_category`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sales`
--
ALTER TABLE `sales`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`product_category_id`) REFERENCES `product_category` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `sales`
--
ALTER TABLE `sales`
  ADD CONSTRAINT `sales_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `sales_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`user_level_id`) REFERENCES `user_level` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `user_ibfk_2` FOREIGN KEY (`user_status_id`) REFERENCES `user_status` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
